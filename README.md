# Overview
My own link list for interesting resources around the net.

##List:
Official apple iOS design resources:  <https://developer.apple.com/design/resources/#ios-apps>

Package manager:  
<https://bower.io/>  
<http://bundler.io>

iOS dev utilities:  
<https://github.com/libimobiledevice>

General technologies:  
<https://opensource.google.com>

Icons:  
<http://adamwhitcroft.com/batch/>  
<https://www.app-bits.com>

APIs:  
<https://www.openapis.org>